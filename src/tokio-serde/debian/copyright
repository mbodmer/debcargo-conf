Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tokio-serde
Upstream-Contact: Carl Lerche <me@carllerche.com>
Source: https://github.com/carllerche/tokio-serde

Files: *
Copyright:
 2017-2018 Carl Lerche <me@carllerche.com>
 2018-2019 Bastian Köcher <git@kchr.de>
 2019-2020 Artem Vorotnikov <artem@vorotnikov.me>
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2022 Blair Noctis <n@sail.ng>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
