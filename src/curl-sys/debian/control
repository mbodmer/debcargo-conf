Source: rust-curl-sys
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-cc-1+default-dev <!nocheck>,
 librust-libc-0.2+default-dev (>= 0.2.2-~~) <!nocheck>,
 librust-openssl-sys-0.9+default-dev <!nocheck>,
 librust-pkg-config-0.3+default-dev (>= 0.3.3-~~) <!nocheck>,
 librust-vcpkg-0.2+default-dev <!nocheck>,
 librust-winapi-0.3+default-dev <!nocheck>,
 librust-winapi-0.3+winsock2-dev <!nocheck>,
 librust-winapi-0.3+ws2def-dev <!nocheck>,
 libcurl4-gnutls-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Ximin Luo <infinity0@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/curl-sys]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/curl-sys
Rules-Requires-Root: no

Package: librust-curl-sys-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-cc-1+default-dev,
 librust-libc-0.2+default-dev (>= 0.2.2-~~),
 librust-pkg-config-0.3+default-dev (>= 0.3.3-~~),
 librust-vcpkg-0.2+default-dev,
 librust-winapi-0.3+default-dev,
 librust-winapi-0.3+winsock2-dev,
 librust-winapi-0.3+ws2def-dev,
 libcurl4-gnutls-dev
Recommends:
 librust-curl-sys+openssl-sys-dev (= ${binary:Version})
Suggests:
 librust-curl-sys+http2-dev (= ${binary:Version})
Provides:
 librust-curl-sys+force-system-lib-on-osx-dev (= ${binary:Version}),
 librust-curl-sys+mesalink-dev (= ${binary:Version}),
 librust-curl-sys+ntlm-dev (= ${binary:Version}),
 librust-curl-sys+poll-7-68-0-dev (= ${binary:Version}),
 librust-curl-sys+protocol-ftp-dev (= ${binary:Version}),
 librust-curl-sys+spnego-dev (= ${binary:Version}),
 librust-curl-sys+static-curl-dev (= ${binary:Version}),
 librust-curl-sys+upkeep-7-62-0-dev (= ${binary:Version}),
 librust-curl-sys+windows-static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0-dev (= ${binary:Version}),
 librust-curl-sys-0+force-system-lib-on-osx-dev (= ${binary:Version}),
 librust-curl-sys-0+mesalink-dev (= ${binary:Version}),
 librust-curl-sys-0+ntlm-dev (= ${binary:Version}),
 librust-curl-sys-0+poll-7-68-0-dev (= ${binary:Version}),
 librust-curl-sys-0+protocol-ftp-dev (= ${binary:Version}),
 librust-curl-sys-0+spnego-dev (= ${binary:Version}),
 librust-curl-sys-0+static-curl-dev (= ${binary:Version}),
 librust-curl-sys-0+upkeep-7-62-0-dev (= ${binary:Version}),
 librust-curl-sys-0+windows-static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4-dev (= ${binary:Version}),
 librust-curl-sys-0.4+force-system-lib-on-osx-dev (= ${binary:Version}),
 librust-curl-sys-0.4+mesalink-dev (= ${binary:Version}),
 librust-curl-sys-0.4+ntlm-dev (= ${binary:Version}),
 librust-curl-sys-0.4+poll-7-68-0-dev (= ${binary:Version}),
 librust-curl-sys-0.4+protocol-ftp-dev (= ${binary:Version}),
 librust-curl-sys-0.4+spnego-dev (= ${binary:Version}),
 librust-curl-sys-0.4+static-curl-dev (= ${binary:Version}),
 librust-curl-sys-0.4+upkeep-7-62-0-dev (= ${binary:Version}),
 librust-curl-sys-0.4+windows-static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+force-system-lib-on-osx-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+mesalink-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+ntlm-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+poll-7-68-0-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+protocol-ftp-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+spnego-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+static-curl-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+upkeep-7-62-0-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+windows-static-ssl-dev (= ${binary:Version})
Description: Native bindings to the libcurl library - Rust source code
 This package contains the source for the Rust curl-sys crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-curl-sys+http2-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-curl-sys-dev (= ${binary:Version}),
 librust-libnghttp2-sys-0.1+default-dev (>= 0.1.3-~~)
Provides:
 librust-curl-sys+libnghttp2-sys-dev (= ${binary:Version}),
 librust-curl-sys-0+http2-dev (= ${binary:Version}),
 librust-curl-sys-0+libnghttp2-sys-dev (= ${binary:Version}),
 librust-curl-sys-0.4+http2-dev (= ${binary:Version}),
 librust-curl-sys-0.4+libnghttp2-sys-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+libnghttp2-sys-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+http2-dev (= ${binary:Version})
Description: Native bindings to the libcurl library - feature "http2" and 1 more
 This metapackage enables feature "http2" for the Rust curl-sys crate,
 by pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "libnghttp2-sys" feature.

Package: librust-curl-sys+openssl-sys-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-curl-sys-dev (= ${binary:Version}),
 librust-openssl-sys-0.9+default-dev
Provides:
 librust-curl-sys+default-dev (= ${binary:Version}),
 librust-curl-sys+ssl-dev (= ${binary:Version}),
 librust-curl-sys+static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0+openssl-sys-dev (= ${binary:Version}),
 librust-curl-sys-0+default-dev (= ${binary:Version}),
 librust-curl-sys-0+ssl-dev (= ${binary:Version}),
 librust-curl-sys-0+static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4+openssl-sys-dev (= ${binary:Version}),
 librust-curl-sys-0.4+default-dev (= ${binary:Version}),
 librust-curl-sys-0.4+ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4+static-ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+openssl-sys-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+default-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+ssl-dev (= ${binary:Version}),
 librust-curl-sys-0.4.58+static-ssl-dev (= ${binary:Version})
Description: Native bindings to the libcurl library - feature "openssl-sys" and 3 more
 This metapackage enables feature "openssl-sys" for the Rust curl-sys crate, by
 pulling in any additional dependencies needed by that feature.
 .
 Additionally, this package also provides the "default", "ssl", and "static-
 ssl" features.
